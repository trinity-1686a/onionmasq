use std::{
    collections::HashMap,
    net::{IpAddr, Ipv4Addr},
    sync::{Arc, Mutex},
};

use bytes::Bytes;
use futures::stream::StreamExt;
use log::{error, info};
use smoltcp::{
    socket::{UdpPacketMetadata, UdpSocketBuffer},
    wire::IpAddress,
};

use crate::socket::UdpSocket;

pub type LockedDnsCookies = Arc<Mutex<DnsCookies>>;

pub struct DnsCookies {
    cookies: HashMap<IpAddr, String>,
    // Reverse cookie. This is so when we get a new hostname, we can quickly learn if we have it in
    // the cookie hashmap or not. XXX: This is not optimal.
    rev_cookies: HashMap<String, IpAddr>,
    next_v4_cookie: Ipv4Addr,
}

impl DnsCookies {
    fn new() -> Self {
        Self {
            cookies: HashMap::new(),
            rev_cookies: HashMap::new(),
            next_v4_cookie: Ipv4Addr::new(1, 0, 0, 1),
        }
    }

    fn next_v4(&mut self, h: String) -> Ipv4Addr {
        let mut hostname = h.clone();
        if hostname.ends_with('.') {
            hostname.pop();
        }

        if let Some(ip) = self.rev_cookies.get(&hostname) {
            match ip {
                IpAddr::V4(v4) => return v4.clone(),
                _ => (),
            }
        }

        info!(
            "Added hostname cookie: {} -> {}",
            self.next_v4_cookie, hostname
        );
        self.cookies
            .insert(self.next_v4_cookie.into(), hostname.clone());
        self.rev_cookies
            .insert(hostname, self.next_v4_cookie.into());

        let current = self.next_v4_cookie;

        // XXX: This is obviously problematic, it needs to be bounded.
        let u_addr: u32 = self.next_v4_cookie.into();
        self.next_v4_cookie = (u_addr + 1).into();

        current
    }

    pub fn get(&self, addr: &IpAddress) -> Option<&String> {
        self.cookies.get(&addr.clone().into())
    }
}

pub struct DnsManager {
    listener_udp_v4: UdpSocket,
    listener_udp_v6: UdpSocket,
    cookies: LockedDnsCookies,
}

impl DnsManager {
    pub fn new(iface: crate::IFace) -> Self {
        Self {
            listener_udp_v4: DnsManager::new_udp_listener(&iface, IpAddress::v4(10, 42, 42, 53)),
            listener_udp_v6: DnsManager::new_udp_listener(
                &iface,
                IpAddress::v6(0xfe80, 0, 0, 0, 0, 0, 0, 53),
            ),
            cookies: Arc::new(Mutex::new(DnsCookies::new())),
        }
    }

    fn new_udp_listener(iface: &crate::IFace, bind_addr: IpAddress) -> UdpSocket {
        let rx_buffer = UdpSocketBuffer::new(vec![UdpPacketMetadata::EMPTY], vec![0; 4096]);
        let tx_buffer = UdpSocketBuffer::new(vec![UdpPacketMetadata::EMPTY], vec![0; 4096]);

        let socket = smoltcp::socket::UdpSocket::new(rx_buffer, tx_buffer);

        let mut listener = UdpSocket::new(iface.clone(), socket);
        listener.bind(bind_addr, 53);

        listener
    }

    pub fn cookies(&self) -> Arc<Mutex<DnsCookies>> {
        self.cookies.clone()
    }

    fn build_answer(&self, query: &dns_message_parser::Dns) -> dns_message_parser::Dns {
        // XXX: This doesn't handle:
        //      - Multiple Questions
        //      - IPv6
        //      - Additionnals
        let q = query.questions.get(0).unwrap();
        let cookie = self
            .cookies
            .lock()
            .unwrap()
            .next_v4(q.domain_name.to_string().clone());
        let a = dns_message_parser::rr::A {
            domain_name: q.domain_name.clone(),
            ttl: 10,
            ipv4_addr: cookie,
        };
        let rr = dns_message_parser::rr::RR::A(a);
        let flags = dns_message_parser::Flags {
            qr: true,
            opcode: dns_message_parser::Opcode::Query,
            aa: false,
            tc: false,
            rd: true,
            ra: true,
            ad: false,
            cd: false,
            rcode: dns_message_parser::RCode::NoError,
        };
        dns_message_parser::Dns {
            id: query.id,
            flags,
            questions: query.questions.clone(),
            answers: vec![rr],
            authorities: query.authorities.clone(),
            additionals: query.additionals.clone(),
        }
    }

    fn decode(&self, payload: Bytes) -> Option<dns_message_parser::Dns> {
        let ret = dns_message_parser::Dns::decode(payload);
        match ret {
            Ok(p) => {
                info!("DNS question: {}", p);
                Some(self.build_answer(&p))
            }
            Err(e) => {
                error!("Unable to parse DNS request: {}", e);
                None
            }
        }
    }

    pub async fn start(&mut self) {
        info!("Starting DNS manager");

        loop {
            tokio::select! {
                r = self.listener_udp_v4.next() => match r {
                    Some((payload, endpoint)) => {
                        if let Some(reply) = self.decode(payload) {
                            let _ = self.listener_udp_v4.send(reply.encode().unwrap().as_ref(), endpoint);
                        }
                    }
                    None => break,
                },
                r = self.listener_udp_v6.next() => match r {
                    Some((payload, endpoint)) => {
                        if let Some(reply) = self.decode(payload) {
                            let _ = self.listener_udp_v6.send(reply.encode().unwrap().as_ref(), endpoint);
                        }
                    }
                    None => break,
                }
            }
        }
    }
}
