package org.torproject.artitoyvpn.vpn;

import static org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status.ERROR;
import static org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status.STOPPED;

import android.app.Notification;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.VpnService;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.system.OsConstants;

import org.torproject.OnionMasq;
import org.torproject.artitoyvpn.capture.StreamCapture;
import org.torproject.artitoyvpn.utils.PreferenceHelper;
import org.torproject.artitoyvpn.utils.VpnNotificationManager;

import java.io.IOException;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Set;

public class ArtiVpnService extends VpnService {
    static final String TAG = ArtiVpnService.class.getSimpleName();
    public static final String ACTION_START_VPN = TAG + ".start";
    public static final String ACTION_STOP_VPN = TAG + ".stop";
    ParcelFileDescriptor fd;
    Thread thread;
    private VpnNotificationManager notificationManager;
    private final static int ALWAYS_ON_MIN_API_LEVEL = Build.VERSION_CODES.N;
    OnionMasq onionmasq;
    private Thread onionThread;

    /** Maximum packet size is constrained by the MTU, which is given as a signed short. */
    private static final int MAX_PACKET_SIZE = Short.MAX_VALUE;

    private final IBinder binder = new ArtiVpnServiceBinder();
    public class ArtiVpnServiceBinder extends Binder {
        ArtiVpnService getService() {
            return ArtiVpnService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = new VpnNotificationManager(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notification notification = notificationManager.buildForegroundServiceNotification();
        startForeground(VpnNotificationManager.ARTI_NOTIFICATION_ID, notification);
        String action = intent != null ? intent.getAction() : "";
        if (action.equals(ACTION_START_VPN) ||
                action.equals("android.net.VpnService") && Build.VERSION.SDK_INT >= ALWAYS_ON_MIN_API_LEVEL) {
                //only always-on feature triggers this
            thread = new Thread(() -> establishVpn());
            thread.run();
        } else if (action.equals(ACTION_STOP_VPN)) {
            stop();
        }
        return START_STICKY;
    }

    @Override
    public void onRevoke() {
        super.onRevoke();
        closeFd();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        notificationManager.cancelNotifications();
        StreamCapture.getInstance().closeIgnoreException();
        if (VpnStatusObservable.getStatus().getValue() != ERROR) {
            VpnStatusObservable.update(STOPPED);
        }
    }

    private void stop() {
        if (thread != null) {
            thread.interrupt();
        }
        closeFd();
        VpnStatusObservable.update(VpnStatusObservable.Status.STOPPING);
        stopForeground(true);
        stopSelf();
    }

    private void closeFd() {
        StreamCapture.getInstance().closeIgnoreException();
        try {
            if (fd != null)
                fd.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Builder prepareVpnProfile() {
        Builder builder = new Builder();
        applyAppFilter(builder);
        builder.setSession("Arti dummy session");
        builder.addRoute("0.0.0.0", 0);
        builder.addRoute("::",0);
        builder.addAddress("10.42.0.8", 16);
        builder.addAddress("fc00::", 7);

        builder.allowFamily(OsConstants.AF_INET);
        builder.allowFamily(OsConstants.AF_INET6);
        builder.setMtu(1500);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            builder.setMetered(false);
        }
        return builder;
    }

    private void establishVpn() {
        try {
            Builder builder = prepareVpnProfile();
            StreamCapture.initPacketUtils(getApplicationContext());
            StreamCapture.setMTU(1500);
            //fd = StreamCapture.getInstance().getCapturedParcelFileDescriptor(builder.establish());
            fd = builder.establish();
            OnionMasq.initLogging();
            onionThread = new Thread(() -> {
                OnionMasq.runProxy(fd.detachFd(), getTunName(), getCacheDir().getAbsolutePath());
            });
            onionThread.start();

            VpnStatusObservable.update(VpnStatusObservable.Status.RUNNING);
        } catch (Exception e) {
            // Catch any exception
            e.printStackTrace();
            VpnStatusObservable.update(ERROR);
            stopSelf();
        }
    }

    private String getTunName() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface networkInterface = en.nextElement();
                if (networkInterface.getName().contains("tun")) {
                    return networkInterface.getName();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    /**
     * Adds selected app into 'allowed apps' for current vpn connection. Only selected apps will use VPN.
     * @param builder VPN Builder
     */
    private void applyAppFilter(Builder builder){
        PreferenceHelper helper = new PreferenceHelper(getApplicationContext());
        Set<String> selectedApps = helper.getSelectedApps();

        if (selectedApps == null){
            //No selection done, so we allow all apps.
            return;
        }

        PackageManager packageManager = getPackageManager();
        for (String appPackage: selectedApps){
            try {
                packageManager.getPackageInfo(appPackage, 0);
                builder.addAllowedApplication(appPackage);
            } catch (PackageManager.NameNotFoundException e) {
                // The app is selected but isn't installed anymore.
            }
        }
    }
}
